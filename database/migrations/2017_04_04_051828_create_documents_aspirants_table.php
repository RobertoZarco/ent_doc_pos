<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_aspirants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aspirant')->unsigned();
            $table->string('document_path',100);
            $table->boolean('digital_document_uploaded');
            $table->boolean('validated_digital_document');
            $table->boolean('physical_document_received');
            $table->boolean('validated_physical_document');
            $table->text('tag');
            $table->string('plan_type_doc',45);


            $table->integer('plan_type_documents_plans_plan_id')->unsigned();
            $table->integer('plan_type_documents_plan_plan_id')->unsigned();
            $table->integer('plan_type_documents_type_documents_document_id')->unsigned();
            
            

          /*  
            $table->foreign('aspirant')
            ->references('id')
            ->on('aspirants')
            ->onDelete('cascade');


            $table->foreign('plan_type_documents_plan_plan_id', 'plan_type_documents_type_documents_document_id')
            ->references('id')
            ->on('plan_type_documents'('plan_plan_id','type_documents_id_documents'))
            ->onDelete('cascade');
        */




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_aspirants');
    }
}
