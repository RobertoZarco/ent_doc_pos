<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
          $table->increments('id');
          $table->boolean('create_users');
          $table->boolean('edit_users');
          $table->boolean('create_aspirant');
          $table->boolean('edit_aspirant');
          $table->boolean('chat');
          $table->boolean('view_digital_document');
          $table->boolean('delete_digital_documento');
          $table->boolean('validate_digital_document');
          $table->boolean('receive_physical_document');
          $table->boolean('validate_physical_document');
          $table->boolean('reports');
          $table->integer('user_id')->unsigned();

          $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

          $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
