<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_surname', 50);
            $table->string('second_surname', 50);
            $table->string('name', 50);
            $table->date('birthdate')->nullable();
            $table->string('curp', 18);
            $table->string('gender', 1);
            $table->string('email')->unique();
            $table->string('login',50);
            $table->string('password', 60);
            $table->boolean('active');
            $table->integer('dependency')->unsigned();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
