<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('municipalities', function (Blueprint $table) {
        $table->increments('id');
        $table->string('code_country',3);
        $table->integer('code_state')->unsigned();
        $table->string('code_municipality',3);
        $table->string('municipality',100);

        $table->foreign('code_state')
          ->references('id')
          ->on('states')
          ->onDelete('cascade');


        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipalities');
    }
}
