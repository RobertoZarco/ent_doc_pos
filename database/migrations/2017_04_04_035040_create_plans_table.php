<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description_plan',100);
            $table->date('creation_date')->nullable();
            $table->integer('minimum_compulsory_credits');
            $table->integer('minimum_optional_credits');
            $table->integer('maximum_duration_tc');
            $table->integer('maximum_duration_tp');
            $table->string('status',1);
            $table->string('level');
            $table->string('objetive',200);
            $table->integer('key_sep');
            $table->string('link');
            $table->string('type_plan',1);

            $table->integer('programs_id_program')->unsigned();
            $table->integer('areas_id_area')->unsigned();


            $table->foreign('programs_id_program')
            ->references('id')
            ->on('programs')
            ->onDelete('cascade');

            $table->foreign('areas_id_area')
            ->references('id')
            ->on('areas')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
