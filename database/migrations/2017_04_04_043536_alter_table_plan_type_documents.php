<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanTypeDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('plan_type_documents', function($table) {
            $table->foreign('plan_plan_id')
            ->references('id')
            ->on('plans')
            ->onDelete('cascade');

            $table->foreign('type_document_document_id')
            ->references('id')
            ->on('type_documents')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
