<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspirants', function (Blueprint $table) {


          $table->increments('id');
          $table->string('reference',12);
          $table->string('account_number',9);
          $table->string('first_surname', 50);
          $table->string('second_surname', 50);
          $table->string('name', 50);
          $table->date('birthdate')->nullable();
          $table->string('curp',18);
          $table->string('gender', 1);
          $table->string('street',100);
          $table->string('outdoor_number',10);
          $table->string('interior_number',10);
          $table->string('colony',50);

          $table->integer('country')->unsigned();
          $table->integer('state')->unsigned();
          $table->integer('municipality')->unsigned();

          $table->string('postal_code',10);
          $table->string('email')->unique();
          $table->string('login',50);
          $table->string('password', 60);
          $table->boolean('validate');
          $table->boolean('active');

          $table->integer('concourse')->unsigned();
          $table->integer('period')->unsigned();
          $table->integer('income_type')->unsigned();
          $table->integer('modality')->unsigned();
          $table->integer('dependency')->unsigned();

          


          $table->foreign('country')
            ->references('id')
            ->on('countries')
            ->onDelete('cascade');


          $table->foreign('concourse')
            ->references('id')
            ->on('concourses')
            ->onDelete('cascade');


          $table->foreign('period')
            ->references('id')
            ->on('periods')
            ->onDelete('cascade');


          $table->foreign('income_type')
            ->references('id')
            ->on('income_types')
            ->onDelete('cascade');


          $table->foreign('modality')
            ->references('id')
            ->on('modalities')
            ->onDelete('cascade');


          $table->foreign('dependency')
            ->references('id')
            ->on('dependencies')
            ->onDelete('cascade');

          //countries *
          //concourses
          //periods
          //income_types
          //modalities
          //dependencies

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspirants');
    }
}
